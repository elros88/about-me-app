package ve.com.disinglab.aboutme

import android.content.Context
import android.hardware.input.InputManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import ve.com.disinglab.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var nick : TextView
    lateinit var nickInput: EditText
    lateinit var putButton : Button
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        nick = binding.nickText
        nickInput = binding.nickInput
        putButton = binding.putButton

        putButton.setOnClickListener { updateNick(it) }
        nick.setOnClickListener { resetNick() }
    }

    private fun resetNick(){
        nickInput.text = nick.editableText
        nick.visibility = View.INVISIBLE
        nickInput.visibility = View.VISIBLE
        putButton.visibility = View.VISIBLE
    }

    private fun updateNick(view: View){
        nick.text = nickInput.text.toString()
        nick.visibility = View.VISIBLE
        nickInput.visibility = View.GONE
        putButton.visibility = View.GONE

        val inputMethod = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethod.hideSoftInputFromWindow(view.windowToken, 0)

    }
}
